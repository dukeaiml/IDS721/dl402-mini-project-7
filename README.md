# IDS 721 Mini Project 7
This project demonstrates the process of working with a vector database using Rust. It covers data ingestion, querying, aggregation, and output visualization within the context of Qdrant.

## Prerequisites

Before you start, ensure you have the following installed:
- Rust and Cargo
- Docker

## Build Process:
Follow these steps to set up and run your project:

### Step 1: Project Setup

1. **Create a New Rust Project:**
   Initialize a new Rust project using Cargo.
   
   ```
   cargo new <PROJECT_NAME>
   ```
2. **Add Dependencies:** 
    Add the required dependencies to your Cargo.toml file by running 

    ```
    cargo add qdrant-client anyhow tonic tokio serde-json --features tokio/rt-multi-thread
    ```
3. ## Add Functionalities:
    Add the code for ingesting data and query data in the main.rs file.

### Step 2: Database Setup

#### Launch Qdrant Database:
   Use Docker to run the Qdrant database with the gRPC interface enabled.

    ```
    docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
    ```

### Step 3: Database Setup
Use `cargo run` to run the project

## Screenshots
- Screenshot of qdrant vector database created: 
![Alt text](/screenshots/qdrant.png)

- The code loops five times to generate and insert five unique data points into a collection. In each iteration, it creates a data point with a vector that varies by multiplying the iteration index i with a sequence of constants to generate distinctive vector values. It also constructs a payload containing a name, age, and metadata, which are uniquely formatted or incremented based on i.
![Alt text](/screenshots/ingest.png)

- This line of code performs a search in the specified collection for points closest to the given vector [0.15, 0.25, 0.35, 0.45]. It limits the results to the top 5 matches and includes each point's payload in the output.

![Alt text](/screenshots/query.png)

- Screenshot of query results:
![Alt text](/screenshots/queryResults.png)


