use anyhow::{Result, anyhow};
use qdrant_client::prelude::*;
use qdrant_client::qdrant::{
    CreateCollection, Filter, SearchPoints, VectorParams, VectorsConfig, Distance,
};
use serde_json::json;
use qdrant_client::qdrant::vectors_config::Config;
use tokio;

#[tokio::main]
async fn main() -> Result<()> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;

    let collection_name = "test_collection";
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.into(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Cosine.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;


    // Ingest multiple points with varied data into the database
    for i in 0..5 {
        let vector = vec![0.1 * i as f32, 0.2 * i as f32, 0.3 * i as f32, 0.4 * i as f32]; // Example vectors
        let payload = json!({
            "name": format!("Name{}", i),
            "age": 20 + i,
            "metadata": {"info": format!("Info{}", i)}
        }).try_into().map_err(|e| anyhow!("Payload conversion error: {:?}", e))?;

        let points = vec![PointStruct::new(i, vector, payload)];
        client.upsert_points(collection_name, None, points, None).await?;
    }

    let search_result = client.search_points(&SearchPoints {
        collection_name: collection_name.into(),
        vector: vec![0.15, 0.25, 0.35, 0.45],
        filter: None,
        limit: 5,
        with_payload: Some(true.into()),
        ..Default::default()
    }).await?;

    // Visualize the output for each found point
    for (index, point) in search_result.result.iter().enumerate() {
        println!("Point {} Payload: {:?}", index + 1, point.payload);
    }

    Ok(())
}